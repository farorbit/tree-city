/*
Copyright (C) 2005 Matthias Braun <matze@braunis.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <config.h>
#include <src/lincity-ng/main.hpp>

#include "PainterGL.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <SDL.h>
#include <SDL_opengl.h>
#include <iostream>
#include <typeinfo>
#include <utility> 
#include <algorithm> 

#include "TextureGL.hpp"

std::string current_component;
std::map<std::string, GLuint> VBO1;

glm::mat4 T = glm::mat4(1.0f);
//glm::mat4 T_bak = glm::mat4(1.0f);
std::vector<glm::mat4> T_bak;
glm::mat4 M = glm::mat4(1.0f);

int wow_a = 0;

GLuint gVBO = -1;
GLuint gTBO = -1;
GLuint gVAO = -1;

std::array<float, 8> crect_arr;//float[8]
std::array<float, 8> cr_arr;

GLuint gVBO2 = -1;
GLuint gVAO2 = -1;

float crect_arr2[8];

GLuint gVBO3 = -1;
GLuint gVAO3 = -1;

GLuint gVBO4 = -1;
GLuint gVAO4 = -1;

float crect_arr4[8];

GLuint gVBO5 = -1;
GLuint gVAO5 = -1;

GLuint gVBO6 = -1;
GLuint gVAO6 = -1;

float crect_arr6[4];

GLuint modelMatIdx = -1;
GLuint viewMatIdx = -1;
GLuint projMatIdx = -1;

float last_rect[8];
float last_r[8];

float last_col[4];

PainterGL::PainterGL()
{
    //glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

PainterGL::~PainterGL()
{
    glDisable(GL_BLEND);
    //glDisable(GL_TEXTURE_2D);
}

void
set_active_color(float r, float g, float b, float a)
{
    if (last_col[0] == r && last_col[1] == g && last_col[2] == b && last_col[3] == a){
        return;
    }else{
        last_col[0] = r;
        last_col[1] = g;
        last_col[2] = b;
        last_col[3] = a;
    }
    if (ProgramIDe != -1){
        int uniformPosition_col = glGetUniformLocation(ProgramIDe, "col");
        glUniform4f(uniformPosition_col, r, g, b, a);
        //glBindFragDataLocation(ProgramIDe, 1, "TextCoord");
        //glUseProgram(ProgramIDe);
    }
}

void
update_matrix()
{
    if (modelMatIdx == -1 || viewMatIdx == -1 || projMatIdx == -1){
        modelMatIdx = glGetUniformLocation(ProgramIDe, "modelMat");
        viewMatIdx = glGetUniformLocation(ProgramIDe, "viewMat");
        projMatIdx = glGetUniformLocation(ProgramIDe, "projMat");
    }
    
    //T = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f, 0.0f, 0.0f));
    glm::mat4 Rx = glm::rotate(T,  0.0f, glm::vec3(1.0f, 0.0f, 0.0f));//rotation_x
    glm::mat4 Ry = glm::rotate(Rx, 0.0f, glm::vec3(0.0f, 1.0f, 0.0f));//rotation_y
    //M = glm::rotate(Ry, 0.0f, glm::vec3(0.0f, 0.0f, 1.0f));//rotation_z
    M = Ry;
    glm::mat4 V = glm::lookAt( glm::vec3( 0.f, 0.f, 1.0f ),glm::vec3( 0.f, 0.f, 0.f ),glm::vec3( 0.0f, 1.0f, 0.0f ) );   
    
    glUniformMatrix4fv(viewMatIdx, 1, GL_FALSE, &V[0][0]);
    glUniformMatrix4fv(modelMatIdx, 1, GL_FALSE, &M[0][0]);
    glUniformMatrix4fv(projMatIdx, 1, GL_FALSE, &projectionMatrix[0][0]);
}

void
PainterGL::drawTextureRect(const Texture* texture, const Rect2D& rect)
{
    const TextureGL* textureGL = static_cast<const TextureGL*> (texture);
    const Rect2D& r = textureGL->rect;
    
    //float* r_arr = r.as_array(); 
    //float* rect_arr = rect.as_array(); 
    
    std::array<float, 8> r_arr;
    std::array<float, 8> rect_arr;
    r_arr[0] = r.p1.x;
    r_arr[1] = r.p1.y;
    r_arr[2] = r.p1.x;
    r_arr[3] = r.p2.y;
    r_arr[4] = r.p2.x;
    r_arr[5] = r.p1.y;
    r_arr[6] = r.p2.x;
    r_arr[7] = r.p2.y;
    
    rect_arr[0] = rect.p1.x;
    rect_arr[1] = rect.p1.y;
    rect_arr[2] = rect.p1.x;
    rect_arr[3] = rect.p2.y;
    rect_arr[4] = rect.p2.x;
    rect_arr[5] = rect.p1.y;
    rect_arr[6] = rect.p2.x;
    rect_arr[7] = rect.p2.y;

    
    glActiveTexture(GL_TEXTURE0);
    
    glBindTexture(GL_TEXTURE_2D, textureGL->handle);
    //glUniform1i(textureGL->handle, 0);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    set_active_color(0.0, 0.0, 0.0, 0.0);
    
    if (gVBO == -1 || gTBO == -1 || gVAO == -1){
        glGenBuffers( 1, &gVBO );
        glGenBuffers( 1, &gTBO );
        glCreateVertexArrays(1, &gVAO);
    }
    
    int new_data = 0;
    int new_tex_data = 0;
    for( int a = 0; a < 6; ++a ) {//only need to check three points
        if (crect_arr[a] != rect_arr[a]){//if a rectangle because if 3 match, so will the fourth
            new_data = 1;
        }
        if (cr_arr[a] != r_arr[a]){
            new_tex_data = 1;
        }
        if (new_data == 1 && new_tex_data == 1){
            break;
        }
    }
    
    glBindVertexArray(gVAO);
    if ((gVBO != -1 && gTBO != -1 && gVAO != -1) && new_data == 1){
        
        //glVertexArrayAttribFormat(gVAO, 0, 3, GL_FLOAT, GL_FALSE, 0);
        /*if (VBO1.find(current_component) != VBO1.end()){//does not work
            gVBO = VBO1[current_component];
            glBindBuffer( GL_ARRAY_BUFFER, gVBO );
        } else{
            glBindBuffer( GL_ARRAY_BUFFER, gVBO );
            glBufferData( GL_ARRAY_BUFFER, sizeof(rect_arr), rect_arr, GL_STATIC_DRAW );// * 4
        }*/
        glBindBuffer( GL_ARRAY_BUFFER, gVBO );
        glBufferData( GL_ARRAY_BUFFER, sizeof(rect_arr), &rect_arr[0], GL_STATIC_DRAW );// * 4
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        //glEnableVertexAttribArray(0);
        
        //glVertexArrayVertexBuffer(gVAO, 0, gVBO, 0, sizeof(float) * 2);
    }
    
    if ((gVBO != -1 && gTBO != -1 && gVAO != -1) && new_tex_data == 1){        
        glBindBuffer(GL_ARRAY_BUFFER, gTBO );
        glBufferData(GL_ARRAY_BUFFER, sizeof(r_arr), &r_arr[0], GL_DYNAMIC_DRAW );// * 4 GLfloat
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
        //glEnableVertexAttribArray(1);
        
        //glBindAttribLocation(ProgramIDe,0, "in_Position");
        glBindAttribLocation(ProgramIDe, 1, "in_UV");
    
    }
    //update_matrix();
    
    if (new_data == 1 || new_tex_data == 1){
        //for(int a = 0; a < 8; ++a ) {
            cr_arr = r_arr;
            crect_arr = rect_arr;
        //}
    }/*else{
        VBO1.insert(std::make_pair(current_component, gVBO));
        glGenBuffers( 1, &gVBO );
    }*/
    
    
    
    //glBindVertexArray(gVAO);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    
    //glClientActiveTexture(GL_TEXTURE0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);//GL_TRIANGLES
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glBindVertexArray(0);
}
void
PainterGL::drawTexture(const Texture* texture, const Vector2& pos)
{
    Rect2D rect(pos, pos + Vector2(texture->getWidth(), texture->getHeight()));
    drawTextureRect(texture, rect);
}

void
PainterGL::drawStretchTexture(Texture* texture, const Rect2D& rect)
{
    assert(typeid(*texture) == typeid(TextureGL));

    if(texture == 0) {
        std::cerr << "Trying to render 0 texture.";
#ifdef DEBUG
        assert(false);
#endif
        return;
    }
    drawTextureRect(texture, rect);
}

void
PainterGL::drawLine( const Vector2 pointA, const Vector2 pointB )
{
    //glDisable(GL_TEXTURE_2D);
    
    float rect_arr[4];
    
    rect_arr[0] = pointA.x;
    rect_arr[1] = pointA.y;
    rect_arr[2] = pointB.x;
    rect_arr[3] = pointB.y;
    
    set_active_color((float)lineColor.r/255.0f, (float)lineColor.g/255.0f, (float)lineColor.b/255.0f, (float)lineColor.a/255.0f);
    
    if (gVBO6 == -1 || gVAO6 == -1){
        glGenBuffers( 1, &gVBO6 );
        
        glCreateVertexArrays(1, &gVAO6);
    }
    
    int new_data = 0;
    for( int a = 0; a < 4; ++a ) {
        if (crect_arr6[a] != rect_arr[a]){
            new_data = 1;
        }
        if (new_data == 1){
            break;
        }
    }
    
    glBindVertexArray(gVAO6);
    if ((gVBO6 != -1 && gVAO6 != -1) && new_data == 1){
        //glVertexArrayAttribFormat(gVAO, 0, 3, GL_FLOAT, GL_FALSE, 0);
        
        glBindBuffer( GL_ARRAY_BUFFER, gVBO6);
        glBufferData( GL_ARRAY_BUFFER, sizeof(rect_arr), rect_arr, GL_STATIC_DRAW );// * 4 8 * 
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        //glEnableVertexAttribArray(0);
        
        //glVertexArrayVertexBuffer(gVAO, 0, gVBO, 0, sizeof(float) * 2);
        
        //glBindAttribLocation(ProgramIDe,0, "in_Position");
        //glBindAttribLocation(ProgramIDe,1, "in_UV");
        
    }
    //update_matrix();
    
    if (new_data == 1){
        for(int a = 0; a < 4; ++a ) {
            crect_arr6[a] = rect_arr[a];
        }
    }
    
    //glEnableClientState(GL_VERTEX_ARRAY);
    //glVertexPointer(2, GL_FLOAT, 2 * sizeof(float), &rect_arr[0]);
    
    //glDisableClientState(GL_VERTEX_ARRAY);
    
    glBindVertexArray(gVAO6);
    glEnableVertexAttribArray(0);
    
    //glEnableClientState(GL_VERTEX_ARRAY);
    //glVertexPointer(2, GL_FLOAT, 2 * sizeof(float), &rect_arr[0]);
    glDrawArrays(GL_LINES, 0,2);
    //glDisableClientState(GL_VERTEX_ARRAY);
    
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);

    //glEnable(GL_TEXTURE_2D);
}

void
PainterGL::fillRectangle(const Rect2D& rect)
{
    //glDisable(GL_TEXTURE_2D);

    //printf("Drawrect: %f %f %f %f.\n", rect.p1.x, rect.p1.y, rect.p2.x, rect.p2.y);

    float rect_arr[8];
    
    rect_arr[0] = rect.p1.x;
    rect_arr[1] = rect.p1.y;
    rect_arr[2] = rect.p1.x;
    rect_arr[3] = rect.p2.y;
    rect_arr[4] = rect.p2.x;
    rect_arr[5] = rect.p1.y;
    rect_arr[6] = rect.p2.x;
    rect_arr[7] = rect.p2.y;
    
    set_active_color((float)fillColor.r/255.0f, (float)fillColor.g/255.0f, (float)fillColor.b/255.0f, (float)fillColor.a/255.0f);
    
    if (gVBO2 == -1 || gVAO2 == -1){
        glGenBuffers( 1, &gVBO2 );
        
        glCreateVertexArrays(1, &gVAO2);
    }
    
    int new_data = 0;
    for( int a = 0; a < 6; ++a ) {//only need to check three points
        if (crect_arr2[a] != rect_arr [a]){//color is seperate so no need to check
            new_data = 1;//if a rectangle because if 3 match, so will the fourth
        }
        if (new_data == 1){
            break;
        }
    }
    
    glBindVertexArray(gVAO2);
    if ((gVBO2 != -1 && gVAO2 != -1) && new_data == 1){
        //glVertexArrayAttribFormat(gVAO, 0, 3, GL_FLOAT, GL_FALSE, 0);
        
        glBindBuffer( GL_ARRAY_BUFFER, gVBO2 );
        glBufferData( GL_ARRAY_BUFFER, sizeof(rect_arr), rect_arr, GL_STATIC_DRAW );// * 4
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        //glEnableVertexAttribArray(0);
        
        //glVertexArrayVertexBuffer(gVAO, 0, gVBO, 0, sizeof(float) * 2);
        
        //glBindAttribLocation(ProgramIDe,0, "in_Position");
        //glBindAttribLocation(ProgramIDe,1, "in_UV");
        
    }
    //update_matrix();
    
    if (new_data == 1){
        for( int a = 0; a < 8; ++a ) {
            crect_arr2[a] = rect_arr[a];
        }
    }
    
    glEnableVertexAttribArray(0);
    
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}

void
PainterGL::drawRectangle(const Rect2D& rect)
{
    //glDisable(GL_TEXTURE_2D);
    
    float rect_arr[7];
    
    rect_arr[0] = rect.p1.x;
    rect_arr[1] = rect.p1.y;
    rect_arr[2] = rect.p1.x;
    rect_arr[3] = rect.p2.y;
    rect_arr[4] = rect.p2.x;
    rect_arr[5] = rect.p2.y;
    rect_arr[6] = rect.p2.x;
    rect_arr[7] = rect.p1.y;
    
    set_active_color((float)lineColor.r/255.0f, (float)lineColor.g/255.0f, (float)lineColor.b/255.0f, (float)lineColor.a/255.0f);
    
    if (gVBO4 == -1 || gVAO4 == -1){
        glGenBuffers( 1, &gVBO4 );
        
        glCreateVertexArrays(1, &gVAO4);
    }
    
    int new_data = 0;
    for( int a = 0; a < 6; ++a ) {//only need to check three points
        if (crect_arr4[a] != rect_arr [a]){//color is seperate so no need to check
            new_data = 1;//if a rectangle because if 3 match, so will the fourth
        }
        if (new_data == 1){
            break;
        }
    }
    
    glBindVertexArray(gVAO4);
    if ((gVBO4 != -1 && gVAO4 != -1) && new_data == 1){
        
        //glVertexArrayAttribFormat(gVAO, 0, 3, GL_FLOAT, GL_FALSE, 0);
        
        glBindBuffer( GL_ARRAY_BUFFER, gVBO4 );
        glBufferData( GL_ARRAY_BUFFER, 8*sizeof(GL_FLOAT), rect_arr, GL_STATIC_DRAW );//2*sizeof(rect_arr)
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        //glEnableVertexAttribArray(0);
        
        //glVertexArrayVertexBuffer(gVAO, 0, gVBO, 0, sizeof(float) * 2);
        
        //glBindAttribLocation(ProgramIDe,0, "in_Position");
        //glBindAttribLocation(ProgramIDe,1, "in_UV");
        
    }
    update_matrix();
    
    if (new_data == 1){
        for( int a = 0; a < 8; ++a ) {
            crect_arr4[a] = rect_arr[a];
        }
    }
    
    glEnableVertexAttribArray(0);
    
    glDrawArrays(GL_LINE_LOOP, 0,4);
    
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}

void
PainterGL::fillPolygon(int numberPoints, const Vector2* points)//rename to fill iso rect
{
    float rect_arr[8];
    
    set_active_color((float)fillColor.r/255.0f, (float)fillColor.g/255.0f, (float)fillColor.b/255.0f, (float)fillColor.a/255.0f);
    
    if (gVBO3 == -1 || gVAO3 == -1){
        glGenBuffers( 1, &gVBO3 );
        glCreateVertexArrays(1, &gVAO3);
    }
    
    rect_arr[0] = points[0].x;//reorder 0, 1, 3, 2
    rect_arr[1] = points[0].y;
    rect_arr[2] = points[1].x;
    rect_arr[3] = points[1].y;
    rect_arr[4] = points[3].x;
    rect_arr[5] = points[3].y;
    rect_arr[6] = points[2].x;
    rect_arr[7] = points[2].y;
    
    glBindVertexArray(gVAO3);
    if (gVBO3 != -1 && gVAO3 != -1){
        
        //glVertexArrayAttribFormat(gVAO, 0, 3, GL_FLOAT, GL_FALSE, 0);
        
        glBindBuffer( GL_ARRAY_BUFFER, gVBO3 );
        glBufferData( GL_ARRAY_BUFFER, sizeof(rect_arr), rect_arr, GL_STATIC_DRAW );// * 4
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        //glEnableVertexAttribArray(0);
        
        //glVertexArrayVertexBuffer(gVAO, 0, gVBO, 0, sizeof(float) * 2);
        
        //glBindAttribLocation(ProgramIDe,0, "in_Position");
        //glBindAttribLocation(ProgramIDe,1, "in_UV");
        
    }
    //update_matrix();
    
    glEnableVertexAttribArray(0);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}

void
PainterGL::drawPolygon(int numberPoints, const Vector2* points)
{
    //glDisable(GL_TEXTURE_2D);
    
    float rect_arr[numberPoints*2];
    
    set_active_color((float)lineColor.r/255.0f, (float)lineColor.g/255.0f, (float)lineColor.b/255.0f, (float)lineColor.a/255.0f);
    
    if (gVBO5 == -1 || gVAO5 == -1){
        glGenBuffers( 1, &gVBO5 );
        
        glCreateVertexArrays(1, &gVAO5);
    }
    
    for( int i = 0; i < numberPoints; i+=1 ){
        rect_arr[(i*2)] = points[i].x;
        rect_arr[(i*2)+1] = points[i].y;
    }
    
    glBindVertexArray(gVAO5);
    if (gVBO5 != -1 && gVAO5 != -1){
        //glVertexArrayAttribFormat(gVAO, 0, 3, GL_FLOAT, GL_FALSE, 0);
        
        glBindBuffer( GL_ARRAY_BUFFER, gVBO5 );
        glBufferData( GL_ARRAY_BUFFER, sizeof(rect_arr), rect_arr, GL_STATIC_DRAW );// * 4
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        //glEnableVertexAttribArray(0);
        
        //glVertexArrayVertexBuffer(gVAO, 0, gVBO, 0, sizeof(float) * 2);
        
        //glBindAttribLocation(ProgramIDe,0, "in_Position");
        //glBindAttribLocation(ProgramIDe,1, "in_UV");
    }
    //update_matrix();
    
    glEnableVertexAttribArray(0);
    
    //for( int i = 0; i < numberPoints; i++ ){
    glDrawArrays(GL_LINE_LOOP, 0, numberPoints);
    //}
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);

    //glEnable(GL_TEXTURE_2D);
}

void
PainterGL::setFillColor(Color color)
{
    fillColor = color;
}

void
PainterGL::setLineColor(Color color)
{
    lineColor = color;
}

void
PainterGL::translate(const Vector2& vec)
{
    T = glm::translate(T,glm::vec3(vec.x, vec.y, 0.0f));//glm::mat4(1.0f)
    update_matrix();
}

void
PainterGL::resettranslate()//rename update matrices
{
    update_matrix();
    //T = glm::translate(T,glm::vec3(0.0f, 0.0f, 0.0f));//glm::mat4(1.0f);
}

void
PainterGL::update_component_name(const std::string& input)
{
    current_component = input;
}

void
PainterGL::pushTransform()
{
    T_bak.push_back(T);
}

void
PainterGL::popTransform()
{
    if (T_bak.size() == 0){
        T = glm::translate(T,glm::vec3(0.0f, 0.0f, 0.0f));//glm::mat4(1.0f);
        update_matrix();
        return;
    }
    T = T_bak[T_bak.size()-1];
    update_matrix();
    T_bak.pop_back();
}

void
PainterGL::setClipRectangle(const Rect2D& rect)
{
    if (gWindow != NULL){
        //GLfloat matrix[16];

        int screenHeight = SDL_GetWindowSurface(gWindow)->h;
        glViewport((GLint) (rect.p1.x + M[3][0]),
                (GLint) (screenHeight - rect.getHeight() - (rect.p1.y + M[3][1])),
                (GLsizei) rect.getWidth(),
                (GLsizei) rect.getHeight());
        projectionMatrix = glm::ortho<float>(rect.p1.x + M[3][0], rect.p1.x + M[3][0] + rect.getWidth(),
                rect.p1.y + M[3][1] + rect.getHeight(),
                rect.p1.y + M[3][1], -1.0f, 1.0f);
        update_matrix();
    }
}

void
PainterGL::clearClipRectangle()
{
    if (gWindow != NULL){
        int width = SDL_GetWindowSurface(gWindow)->w;
        int height = SDL_GetWindowSurface(gWindow)->h;
        glViewport(0, 0, width, height);
        
        projectionMatrix = glm::ortho<float>(0.0, 1.0f*width, 1.0f*height, 0.0);
        update_matrix();
    }
}

Painter*
PainterGL::createTexturePainter(Texture* texture)
{
    (void) texture;
    // TODO
    return 0;
}


/** @file gui/PainterGL/PainterGL.cpp */

