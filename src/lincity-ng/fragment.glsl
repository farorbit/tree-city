#version 330
uniform vec4 col = vec4(0.0,0.0,0.0,0.0);
uniform sampler2D tex;
//layout (location = 1) in vec4 TextCoord;
in vec4 TCoord;
void main(){   
    vec4 color2 = texture(tex,TCoord.xy);//gl_TexCoord[0] 
    if (color2.w == 0.0 && col.a == 0.0){
    	discard;
    }
    if (color2.x != 0.0 || color2.y != 0.0 || color2.z != 0.0 || color2.w != 0.0){
    	gl_FragColor = color2;
    }else{
    	gl_FragColor = vec4(col.r, col.g, col.b, col.a);
    }
} 
