#version 330
in vec4 in_Position; 
in vec4 in_UV; 
uniform mat4 projMat;
uniform mat4 viewMat;
uniform mat4 modelMat;
out vec4 TCoord;
void main() {
    TCoord = in_UV;
    gl_Position = projMat * viewMat * modelMat * in_Position;
}
